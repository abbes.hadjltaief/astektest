# Test

## I- Environnement de Travail
Linux : Ubuntu 20.04.2 LTS

## Install project via Docker
1. Install [Docker](https://docs.docker.com/engine/installation/linux/docker-ce/ubuntu/)
2. Install [Docker Compose](https://docs.docker.com/engine/installation/linux/docker-ce/ubuntu/)

## Use in local

1. run project
```bash
docker-compose up
```
2. get nginx host
```bash
docker inspect -f '{{range .NetworkSettings.Networks}}{{.IPAddress}}{{end}}' nginx
```
3. use this host to access to app1 and app2
you can use curl or navigator to test
```bash
curl http://172.25.0.8:8081/app1
```
```bash
curl http://172.25.0.8:8082/app2
```

`PS: docker-compose in projectA and projectB is used to test individually services.`